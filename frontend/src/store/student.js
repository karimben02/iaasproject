import Vue from "vue"
import axios from 'axios'
import axiosOverlay from "../js/axiosOverlay";

export const storeStudents = Vue.observable({
    students: null,
    selectedStudent : null
});

export const actionsStudents = {
    getActionById (actionID) {
        if (storeActions.actions !== null) {
            return JSON.parse(storeActions.actions).filter(action => action.id === actionID)[0]
        }
    }
}

export const mutationsStudents = {
    loadStudentsFromAPI (){
        let config = {
            method: 'get',
            url: process.env.URL_API + "/students",
            headers: { }
        };

        axiosOverlay(axios(config))
            .then(function (response) {
                console.log(JSON.stringify(response))
                // let result = JSON.stringify(response.data['hydra:member'])
                // // console.log(result)
                // storeActions.actions = result
            })
            .catch(function (error) {
                console.log(error);
            });

    },
    setSelectedAction (actionID) {
        storeActions.selectedAction = actionID
    }
}
