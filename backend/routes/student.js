const express = require('express');
const router = express.Router();
const ObjectID = require('mongoose').Types.ObjectId;
const VerifyToken = require('../jwt');

const Student = require('../models/student');
const Bde = require('../models/bde');
const Promo = require('../models/promo');

router.get('/', VerifyToken, (req, res) => {
    Student.find((err, docs) => {
        if (!err) res.send(docs);
        else return res.status(400).send(err);
    })
});

router.get('/:id', VerifyToken, async (req, res) => {
    try {
        let student = await Student.findById(req.params.id);
        return res.status(200).json(student);
    } catch (error) {
        return res.status(400).send("ID inconnu : " + req.params.id)
    }
});

router.get('/:id/bde', VerifyToken, async (req, res) => {
    try {
        const student = await Student.findById(req.params.id).populate('bde');
        return res.status(200).json(student.bde);
    } catch (error) {
        return res.status(400).send("ID inconnu : " + req.params.id)
    }
});

router.post('/', VerifyToken, (req, res) => {
    const newRecord = new Student({
        nom: req.body.nom,
        prenom: req.body.prenom,
        adresse: req.body.adresse,
        dateInscription : req.body.dateInscription,
        adresse : req.body.adresse,
        promo : req.body.promo,
        mail : req.body.mail,
        bde : req.body.bde
    });

    newRecord.save(async (err, docs) => {
        if (!err) {
            if (req.body.bde){
                try {
                    const bde = await Bde.findById(req.body.bde);

                    bde.members.push(docs["_id"]);
                    await bde.save();
                } catch (error) {
                    return res.status(400).send("ID inconnu : " + req.params.bde)
                }

            }
            if (req.body.promo){
                try {
                    const promo = await Promo.findById(req.body.promo);

                    promo.students.push(docs["_id"]);
                    await promo.save();
                } catch (error) {
                    return res.status(400).send("ID inconnu : " + req.params.promo)
                }

            }
            res.send(docs);
        }
        else return res.status(400).send(err);
    })
});

router.put("/:id", VerifyToken, (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID inconnu : " + req.params.id)

    const updateRecord = {
        nom: req.body.nom,
        prenom: req.body.prenom,
        adresse: req.body.adresse,
        dateInscription : req.body.dateInscription,
        adresse : req.body.adresse,
        promo : req.body.promo,
        mail : req.body.mail
        // bde : req.body.bde
    };

    Student.findByIdAndUpdate(
        req.params.id,
        { $set: updateRecord},
        { new: true },
        (err, docs) => {
            if (!err) res.send(docs);
            else return res.status(400).send(err);
        }
    )
});

router.delete("/:id", VerifyToken, (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID inconnu : " + req.params.id)

    Student.findByIdAndRemove(
        req.params.id,
        (err, docs) => {
            if (!err) res.send(docs);
            else return res.status(400).send(err);
        })
});

module.exports = router;