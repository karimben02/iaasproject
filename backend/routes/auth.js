const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const secret = process.env.SECRET_JWT
const VerifyToken = require('../jwt');


const User = require('../models/user');

router.post('/register', function(req, res) {

    if (!req.body.password) return res.status(500).send("Erreur : vous n'avez pas fourni de mot de passe")

    const hashedPassword = bcrypt.hashSync(req.body.password, 8);

    const newRecord = new User({
        login : req.body.login,
        mail : req.body.mail,
        password : hashedPassword
    });

    newRecord.save((err, docs) => {
        if (err) return res.status(500).send("Erreur : " + err)
        // create a token
        const token = jwt.sign({ id: docs._id }, secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        res.status(200).send({ auth: true, token: token });
        // if (!err) res.send(docs);
        // else return res.status(400).send(err);
    })
});

router.get('/accounts', function(req, res) {
    User.find((err, docs) => {
        if (!err) res.send(docs);
        else return res.status(400).send(err);
    })
});

router.post('/login', function(req, res) {

    User.findOne({ login: req.body.login }, function (err, user) {
        if (err) return res.status(500).send('Erreur : ' + err);
        if (!user) return res.status(404).send('Utilisateur non trouvé');

        var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
        if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });

        var token = jwt.sign({ id: user._id }, secret, {
            expiresIn: 86400 // expires in 24 hours
        });

        res.status(200).send({ auth: true, token: token });
    });

});


router.get('/logout', function(req, res) {
    res.status(200).send({ auth: false, token: null });
});

module.exports = router;