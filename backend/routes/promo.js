const express = require('express');
const router = express.Router();
const ObjectID = require('mongoose').Types.ObjectId;
const VerifyToken = require('../jwt');

const Promo = require('../models/promo');
const Employee = require('../models/employee');

router.get('/', VerifyToken, (req, res) => {
    Promo.find((err, docs) => {
        if (!err) res.send(docs);
        else return res.status(400).send(err);
    })
});

router.get('/:id', VerifyToken, async (req, res) => {
    try {
        let promo = await Promo.findById(req.params.id);
        return res.status(200).json(promo);
    } catch (error) {
        return res.status(400).send("ID inconnu : " + req.params.id)
    }
});

router.post('/', VerifyToken, (req, res) => {
    const newRecord = new Promo({
        nom: req.body.nom,
        responsable: req.body.responsable,
        // bde: req.body.bde,
        students : req.body.students
    });

    newRecord.save(async(err, docs) => {
        if (!err) {
            if (req.body.responsable){
                try {
                    const responsable = await Employee.findById(req.body.responsable);

                    responsable.promos.push(docs["_id"]);
                    await responsable.save();
                } catch (error) {
                    return res.status(400).send("ID inconnu : " + req.params.responsable)
                }

            }
            res.send(docs);
        }
        else return res.status(400).send(err);
    })
});

router.get('/:id/students', VerifyToken, async (req, res) => {
    try {
        const promo = await Promo.findById(req.params.id).populate('students');
        return res.status(200).json(promo.students);
    } catch (error) {
        return res.status(400).send("ID inconnu : " + req.params.id)
    }
});

router.put("/:id", VerifyToken, (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID inconnu : " + req.params.id)

    const updateRecord = {
        nom: req.body.nom,
        responsable: req.body.responsable,
        // bde: req.body.bde,
        students : req.body.students
    };

    Promo.findByIdAndUpdate(
        req.params.id,
        { $set: updateRecord},
        { new: true },
        (err, docs) => {
            if (!err) res.send(docs);
            else return res.status(400).send(err);
        }
    )
});

router.delete("/:id", VerifyToken, (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID inconnu : " + req.params.id)

    Promo.findByIdAndRemove(
        req.params.id,
        (err, docs) => {
            if (!err) res.send(docs);
            else return res.status(400).send(err);
        })
});

module.exports = router;