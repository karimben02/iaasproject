const express = require('express');
const router = express.Router();
const ObjectID = require('mongoose').Types.ObjectId;
const VerifyToken = require('../jwt');

const Bde = require('../models/bde');

router.get('/', VerifyToken, (req, res) => {
    Bde.find((err, docs) => {
        if (!err) res.send(docs);
        else return res.status(400).send(err);
    })
});

router.get('/:id', VerifyToken, async (req, res) => {
    try {
        let bde = await Bde.findById(req.params.id);
        return res.status(200).json(bde);
    } catch (error) {
        return res.status(400).send("ID inconnu : " + req.params.id)
    }
});

router.get('/:id/members', VerifyToken, async (req, res) => {
    try {
        const bde = await Bde.findById(req.params.id).populate('members');
        return res.status(200).json(bde.members);
    } catch (error) {
        return res.status(400).send("ID inconnu : " + req.params.id)
    }
});

router.get('/:id/responsable', VerifyToken, async (req, res) => {
    try {
        const bde = await Bde.findById(req.params.id).populate('responsable');
        return res.status(200).json(bde.responsable);
    } catch (error) {
        return res.status(400).send("ID inconnu : " + req.params.id)
    }
});


router.post('/', VerifyToken, async (req, res) => {
    const newRecord = new Bde({
        nom: req.body.nom,
        responsable: req.body.responsable,
        promo: req.body.promo
    });

    await newRecord.save(async (err, docs) => {
        if (!err) res.send(docs)
        else return res.status(400).send(err);
    })
});

router.put("/:id", (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID inconnu : " + req.params.id)

    const updateRecord = {
        nom: req.body.nom,
        responsable: req.body.responsable,
        promo: req.body.promo
    };

    Bde.findByIdAndUpdate(
        req.params.id,
        { $set: updateRecord},
        { new: true },
        (err, docs) => {
            if (!err) res.send(docs);
            else return res.status(400).send(err);
        }
    )
});

router.delete("/:id", VerifyToken, (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID inconnu : " + req.params.id)

    Bde.findByIdAndRemove(
        req.params.id,
        (err, docs) => {
            if (!err) res.send(docs);
            else return res.status(400).send(err);
        })
});

module.exports = router;