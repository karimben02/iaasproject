const express = require('express');
const router = express.Router();
const ObjectID = require('mongoose').Types.ObjectId;
const VerifyToken = require('../jwt');

const Employee = require('../models/employee');

router.get('/', VerifyToken, (req, res) => {
    Employee.find((err, docs) => {
        if (!err) res.send(docs);
        else return res.status(400).send(err);
    })
});

router.get('/:id', VerifyToken, async (req, res) => {
    try {
        let employee = await Employee.findById(req.params.id);
        return res.status(200).json(employee);
    } catch (error) {
        return res.status(400).send("ID inconnu : " + req.params.id)
    }
});

router.get('/:id/promos', VerifyToken, async (req, res) => {
    try {
        const employee = await Employee.findById(req.params.id).populate('promos');
        return res.status(200).json(employee.promos);
    } catch (error) {
        return res.status(400).send("ID inconnu : " + req.params.id)
    }
});

router.post('/', VerifyToken, (req, res) => {
    const newRecord = new Employee({
        nom: req.body.nom,
        prenom: req.body.prenom,
        promos: req.body.promos,
        mail : req.body.mail
    });

    newRecord.save((err, docs) => {
        if (!err) res.send(docs);
        else return res.status(400).send(err);
    })
});

router.put("/:id", VerifyToken, (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID inconnu : " + req.params.id)

    const updateRecord = {
        nom: req.body.nom,
        prenom: req.body.prenom,
        promos: req.body.promos,
        mail : req.body.mail
    };

    Employee.findByIdAndUpdate(
        req.params.id,
        { $set: updateRecord},
        { new: true },
        (err, docs) => {
            if (!err) res.send(docs);
            else return res.status(400).send(err);
        }
    )
});

router.delete("/:id", VerifyToken, (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID inconnu : " + req.params.id)

    Employee.findByIdAndRemove(
        req.params.id,
        (err, docs) => {
            if (!err) res.send(docs);
            else return res.status(400).send(err);
        })
});

module.exports = router;