const jwt = require('jsonwebtoken');
const secret = process.env.SECRET_JWT

function verifyToken(req, res, next) {
    let token = req.headers['x-access-token'] || req.headers['authorization'];

    if (token) {
        token = token.replace(/^Bearer\s+/, "");

        jwt.verify(token, secret, (err, decoded) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Token is not valid'
                });
            }
            req.decoded = decoded;
            next();
        });
    } else {
        return res.json({
            success: false,
            message: 'Token not provided'
        });
    }
}

module.exports = verifyToken;