const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
const app = express();
require('./db/mongo');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const studentRouter = require('./routes/student');
const promoRouter = require('./routes/promo');
const bdeRouter = require('./routes/bde');
const employeeRouter = require('./routes/employee');
const authRouter = require('./routes/auth');

mongoose.set('useFindAndModify', false);

app.use(bodyParser.json());
app.use(cors());
app.use('/students', studentRouter);
app.use('/promos', promoRouter);
app.use('/bdes', bdeRouter);
app.use('/employees', employeeRouter);
app.use('/', authRouter);


app.use(function(req, res, next) {
    res.status(200).json({
        name   : 'API',
        version: '1.0',
        status : 200,
        message: 'Vous êtes sur le projet API de KIT Anthony et GILLOT Yann, Bienvenue !'
    });
});

app.listen(process.env.PORT, () => console.log('Server started: ' + process.env.PORT));