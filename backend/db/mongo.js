const mongoose = require('mongoose');

mongoose.connect(
    process.env.DB_HOST,
    { useCreateIndex: true,useNewUrlParser: true, useUnifiedTopology: true },
    (err) => {
        if (!err) console.log('La base de donnée est bien connectée');
        else console.log("Connection error :" + err);
    }
)