const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const Student = new Schema({
    nom: {
        type     : String,
        trim     : true,
        required : [true, 'Le nom est obligatoire']
    },
    prenom: {
        type     : String,
        trim     : true,
        required : [true, 'Le prenom est obligatoire']
    },
    adresse: {
        type     : String,
        trim     : true,
        required : [true, 'L\'adresse est obligatoire']
    },
    mail: {
        type     : String,
        trim     : true,
        required : [true, 'L\'adresse mail est obligatoire']
    },
    dateInscription: {
        type     : Date,
        required : [true, 'La date d\inscription est obligatoire']
    },
    promo: {
        type : Schema.Types.ObjectId,
        ref : "Promo",
        required : [true, 'La promo est obligatoire']
    },
    bde: {
        type : Schema.Types.ObjectId,
        ref : "Bde",
    }
}, {
    adresse: true
});

module.exports = mongoose.model('Student', Student);