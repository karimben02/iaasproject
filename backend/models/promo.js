const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const Promo = new Schema({
    nom: {
        type     : String,
        trim     : true,
        required : [true, 'Le nom est obligatoire']
    },
    responsable: {
        type : Schema.Types.ObjectId,
        ref: 'Employee',
        required : [true, 'Le responsable est obligatoire']
    },
    // bde: {
    //     type : Schema.Types.ObjectId,
    //     ref: 'Bde'
    // },
    students: [{
        type : Schema.Types.ObjectId,
        ref: 'Student'
    }]
}, {
    adresse: true
});

module.exports = mongoose.model('Promo', Promo);