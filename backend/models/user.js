const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const User = new Schema({
    login: {
        type     : String,
        trim     : true,
        required : [true, 'Le login est obligatoire'],
        unique : true
    },
    password: {
        type     : String,
        trim     : true,
        required : [true, 'Le mot de passe est obligatoire']
    },
    mail: {
        type     : String,
        trim     : true,
        required : [true, 'L\'adresse mail est obligatoire']
    }
}, {
    adresse: true
});

module.exports = mongoose.model('User', User);