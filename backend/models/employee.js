const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const Employee = new Schema({
    nom: {
        type     : String,
        trim     : true,
        required : [true, 'Le nom est obligatoire']
    },
    prenom: {
        type     : String,
        trim     : true,
        required : [true, 'Le prenom est obligatoire']
    },
    mail: {
        type     : String,
        trim     : true,
        required : [true, 'L\'adresse mail est obligatoire']
    },
    promos: [{
        type : Schema.Types.ObjectId,
        ref : 'Promo'
    }]
}, {
    adresse: true
});

module.exports = mongoose.model('Employee', Employee);