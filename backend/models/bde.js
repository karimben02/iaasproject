const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const Bde = new Schema({
    nom: {
        type     : String,
        trim     : true,
        required : [true, 'Le nom est obligatoire']
    },
    responsable: {
        type : Schema.Types.ObjectId,
        ref: 'Student',
        required : [true, 'Le responsable est obligatoire']
    },
    promo: {
        type : Schema.Types.ObjectId,
        ref: 'Promo',
        required : [true, 'La promo est obligatoire']
    },
    members: [{
        type : Schema.Types.ObjectId,
        ref: 'Student'
    }]
}, {
    adresse: true
});

module.exports = mongoose.model('Bde', Bde);