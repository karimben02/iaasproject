# Projet IAAS

Vous trouverez ici l'explication du projet IAAS d'Anthony Kit et Yann Gillot


## API - IP : 35.189.72.85

Tout les champs marqué d'un astérix sont obligatoires

### Paramètres de chaques données:

Etudiant :
-	
-	nom*
-	prenom*
-	adresse*
-	mail*
-	dateInscription (YYY-MM-JJ)*
-	promo (son id) *
-	bde (son id)

Promotion :
-
-	nom*
-	responsable* (id employé)
-	students (tableau d'id d'etudiants)


Employé :
-
-	nom*
-	prenom*
-	mail*
-	promos  (tableau d'id de promos)

BDE :
-
-	nom*
-	responsable* (id d'etudiant)
-	promo* (id de promo)
-	members (tableau d'id d'etudiants)


Lorsque le champs correspond à un tableau d'id, il n'est pas possible de l'initialiser via une requête POST. Ce tableau se remplira automatiquement quand le lien sera créé de l'autre côté. Cependant, par manque de temps, nous avons pas fait la mise à jour de ce lien en cas de Delete ou de Put par manque de temps. (Bien que la méthode reste la même qu'à la création du lien via un POST)

### Requetes:

(Une collection Postman est mise à votre disposition pour faciliter les tests. Il faudra modifier la variable url de la collection si besoin (clic droit sur la collection -> editer -> variables), puis utiliser la requête "Register" pour obtenir un token, et enfin retourner dans les variables pour mettre à jour la variable de collection du token. 
Les routes register, login et getaccounts sont là en guise de test pour démontrer le fonctionnement d'un token. Elle ne sont donc pas protéger par un token. Les autres le sont 

Maintenant que vous avez votre token, vous avez un dossier par entité et pour chaques entités un getAll, un getID, un Post ainsi que d'autres éventuelles requêtes disponnibles. Pour les PUT, utiliser une requete post et remplacer le prefix par PUT et pour les DELETE, utiliser une requete getById et remplacer par DELETE.
Attention à bien changer les ids dans les urls

En cas de question n'hesitez pas à nous contacter via discord, on saura se rendre disponnible pour expliquer.



## Front - IP : 127.0.0.1:8080

```
	npm install
``` 


